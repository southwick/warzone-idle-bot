# Warzone Idle bot
This program is designed for farming XP and battle pass progression on Modern Warfare (2019) 
using Warzone's Battle Royale gamemode by continuously queuing for matches and moving around 
in game to avoid kicks due to inactivity.  
The bot scans for different UI elements & prompts throughout the game to navigate menus.

## Setup
* Install Python 3.8 **32-bit** or lower from https://www.python.org/downloads/. Check the option
"Add to PATH" to be able to launch python from the commandline.
* Install the packages required by the program using PIP with the command
"pip install ..." (or "pip3 install..."). The required packages are:
    * pypiwin32 : for access to the Windows API (windowing, input, ...).
    * pillow : used to take screenshots of the desktop.
    * opencv-python : used for image analysis. (not compatible with python 3.9)
* Download this program.
* Set Modern Warfare's language to English, as this is the UI language scanned for by this bot.
    * If you want the watchdog feature (that restarts the game in case of freezes or crashes),
    you also need to set the Battle.net launcher's language and Windows' display language to english.
* Set Modern Warfare to run in 720p resolution in Windowed mode (Fullscreen Windowed also works).
Since the game does not let you set a specific resolution in the game options in Windowed mode
(since it lets you resize the game's window), resize the window until "Render Resolution"
(which should be set at 100%) indicates "1280x720" (see image below). Also note that a render resolution of 1280x720 is NOT the requirement, 
the game itself as a whole must run at that resolution.
![render-resolution-1280x720](./Docs/render_resolution.png)
* Make sure that the "jump" button is bound to "SPACE" so that the idler can drop from the ship.

## How to run
Simply start the game, and once it is up, run the program in its root directory using the command 
"python bot.py" (or "python3 bot.py"). The bot should start its keypresses 5 seconds after launch.  
You can specify to the bot which gamemode (Battle royale, Plunder, Blood Money, ...) you wish to queue for.
To specify the gamemode, simply provide the name of the gamemode as an argument (eg "python bot.py br-solos"
to queue for the Battle royale solos gamemode).  
The different gamemodes that are supported are:
* "br-quads" for the Battle Royale Quads gamemode.
* "br-trios" for the Battle Royale Trios gamemode.
* "br-duos" for the Battle Royale Duos gamemode.
* "br-solos" for the Battle Royale Solos gamemode.
* "plunder-trios" for the Plunder Trios gamemode.
* "armored-royale-quads" for the Armored Royale Quads gamemode.

Note that in Plunder, Blood Money and Warzone Rumble gamemodes, it is necessary to pick a loadout.
The bot will look for a loadout named "My loadout" (See image below),
therefore if you wish to queue for these gamemodes, name one of your loadouts "My loadout".  
![My-loadout](./Docs/my_loadout_example.png)

The bot defaults to the Battle Royale Quads gamemode.
